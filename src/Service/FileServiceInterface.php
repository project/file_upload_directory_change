<?php

namespace Drupal\file_upload_directory_change\Service;

Interface FileServiceInterface {

  /**
   * Change the file uri to private/public.
   *
   * @param $uri
   *    Uri of a file.
   *
   * @return mixed
   *    New uri.
   */
  public function changeFilePath($uri);

  /**
   * Change the directory of the file.
   *
   * @param $fid
   *    Drupal file id.
   */
  public function directoryChange($fid);

}