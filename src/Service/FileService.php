<?php

namespace Drupal\file_upload_directory_change\Service;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;

class FileService implements FileServiceInterface {

  /**
   * Drupal file_system service.
   *
   * @var FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function changeFilePath($uri) {
    if (strpos($uri, 'public://') !== FALSE) {
      $uri = str_replace('public://', 'private://', $uri);
    }
    else if (strpos($uri, 'private://') !== FALSE) {
      $uri = str_replace('private://', 'public://', $uri);
    }
    return $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function directoryChange($fid) {
    $file = File::load($fid);
    $uri = $file->getFileUri();
    $new_uri = $this->changeFilePath($uri);
    $directory = $this->fileSystem->dirname($new_uri);

    // Check if the new directory exists.
    if (!file_exists($directory)) {
      $this->fileSystem->mkdir($directory, NULL, TRUE);
    }

    // If current file exists.
    if (realpath($this->fileSystem->realpath($file->getFileUri()))) {
      // Move file
      file_move($file, $new_uri);
    }
    else {
      // if current file not exists
      // and destination file exists
      // then : map old not existing file to new destination without file moving.
      if ($this->fileSystem->realpath($new_uri)) {
        $file->setFileUri($new_uri);
        $file->save();
      }
    }
  }

}