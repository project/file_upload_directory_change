<?php

namespace Drupal\file_upload_directory_change;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\file\Entity\File;

/**
 * Defines methods for integrating file upload directory change into file field widgets.
 */
class FileUploadDirectoryChangeField {

  /**
   * Returns a list of supported widgets.
   */
  public static function supportedWidgets() {
    $widgets = &drupal_static(__FUNCTION__);
    if (!isset($widgets)) {
      $widgets = ['file_generic', 'image_image'];
      \Drupal::moduleHandler()->alter('file_upload_directory_change_supported_widgets', $widgets);
      $widgets = array_unique($widgets);
    }
    return $widgets;
  }

  /**
   * Checks if a widget is supported.
   */
  public static function isWidgetSupported(WidgetInterface $widget) {
    return in_array($widget->getPluginId(), static::supportedWidgets());
  }

  /**
   * Returns widget settings form.
   */
  public static function widgetSettingsForm(WidgetInterface $widget, FieldDefinitionInterface $field_definition, $form_mode, $form, FormStateInterface $form_state) {
    $form = [];
    if (static::isWidgetSupported($widget)) {
      $private_file_path = \Drupal::service('file_system')->realpath("private://");
      if (!$private_file_path) {
        $form['enabled_help'] = [
          '#prefix' => '<div>',
          '#suffix' => '</div>',
          '#markup' => t('A private folder is not define. Please set the key "file_private_path" in sites/default/settings.php and clear the Drupal cache.')
        ];
      }
      else {
        $form['enabled'] = [
          '#type' => 'checkbox',
          '#title' => t('Allow users to change the file location for this field (public/private).'),
          '#default_value' => $widget->getThirdPartySetting('file_upload_directory_change', 'enabled'),
        ];
      }
    }
    return $form;
  }

  /**
   * Alters the summary of widget settings form.
   */
  public static function alterWidgetSettingsSummary(&$summary, $context) {
    /** @var \Drupal\Core\Field\Plugin\Field\FieldWidget\BooleanCheckboxWidget $widget */
    $widget = $context['widget'];
    if (static::isWidgetSupported($widget)) {
      $status = $widget->getThirdPartySetting('file_upload_directory_change', 'enabled') ? t('Yes') : t('No');
      $summary[] = t('File upload directory change enabled: @status', ['@status' => $status]);
    }
  }

  /**
   * Processes widget form.
   */
  public static function processWidget($element, FormStateInterface $form_state, $form) {
    if (isset($element['#default_value']) && isset($element['#default_value']['target_id'])) {
      $file = \Drupal\file\Entity\File::load($element['#default_value']['target_id']);
      $uri = $file->getFileUri();

      /** @var \Drupal\file_upload_directory_change\Service\FileServiceInterface $fudc_file_service */
      $fudc_file_service =  \Drupal::service('file_upload_directory_change.file_service');
      $new_uri = $fudc_file_service->changeFilePath($uri);

      // Path input
      $element['file_upload_directory_change_location'] = [
        '#title' => t('Change file location to @location', ['@location' => $new_uri]),
        '#type' => 'checkbox',
        '#value' => '',
        '#default_value' => '',
      ];
    }
    return $element;
  }

  /**
   * Sets widget file id values by validating and processing the submitted data.
   * Runs before processor callbacks.
   */
  public static function setWidgetValue($element, &$input, FormStateInterface $form_state) {
    if (!isset($input['file_upload_directory_change_location']) || $input['file_upload_directory_change_location'] !== '1') {
      return;
    }

    /** @var \Drupal\file_upload_directory_change\Service\FileServiceInterface $fudc_file_service */
    $fudc_file_service =  \Drupal::service('file_upload_directory_change.file_service');
    foreach($input['fids'] as $fid) {
      $fudc_file_service->directoryChange($fid);
    }
  }

}